# Plaid Party Planning III 2

## Intro
This is part 2 of Plaid Party Planning III. The first part was easy, it had a
puzzle which could be ignored, since it also had a function that decrypts the
flag with a constant key, so we could just jump there and let it give us a flag.

In part 2, an actual solution to the puzzle is required, because the flag is
encrypted using the expected solution to the puzzle.
  
## The Puzzle
We reversed most of the binary to understand the puzzle. 15 named guests came to
have Indian food. We have a round table divided into 15 "pizza slices", in front
of each slice there is a seat, so that each guest has a place to sit. The round
table has 5 rings, on each ring there are different plates of food. We'll number
the rings from 0 to 4. Each plate on a ring is aligned to a "pizza slice".
Here's a diagram (of a smaller table, that has only 8 seats and 3 rings):

![](table.png)

Once all the guests are seated, they start eating at the same time. Each guest
has a predefined eating sequence. For example, guest "tylerni7" will do the
following:

1. Grab plate from ring 1
2. Grab plate from ring 3
3. Grab plate from ring 2
4. Return plate to ring 2 (the plate that he originally took from ring 2)
5. Return plate to ring 1
6. Grab plate from ring 0
7. Return plate to ring 3
8. Return plate to ring 0
9. Grab plate from ring 4
10. Return plate to ring 4

When guests try to grab a plate from a ring, they reach for the nearest plate.
If they have 2 plates with an equal distance, they will grab the plate to the
right. For example, if we look at the illustration:

![](table.png)

- If G4 wants to take a plate from ring 2, he will take the plate in front of G3.
- If G1 wants to take a plate from ring 1, he will take the plate directly in
  front of him.
- If G3 wants the take a plate from ring 0, he will take the plate in front on
  G4.

As we mentioned before, the guests are eating simultaneously (it's implemented
by running each guest in their own thread), so what happens if two guests try to
grab the same plate? Actually, the second guest will wait for the first guest to
return the plate (implemented with a mutex). An important thing to note: The
guest always tries to grab the nearest plate, even when it's taken and even when
there's another plate available in the same ring! For example, if G3 grabs a
plate from ring 3 (the plate in front of him), and G2 also wants a plate from
ring 3, G2 will wait for G3 to return the plate, even if the plate in front of
G1 is available.

The program accepts a seating arrangement for the guests as input.

## The Problem
All looks fine, but when we run the program with the default alphabetical guest
arrangement the program crashes. Looking at the source of the crash in GDB,
looks like guest "ricky" doesn't want to sit close to a plate of paneer in
ring 2. (ricky's eating program calls abort() if he has paneer in ring 2). OK...

If we try another arrangement where ricky is far away from the paneer we see
that the program hangs due to a deadlock.

Let's look again at the illustration:

![](table.png)

Assume G1's eating sequence is
1. Take plate, ring 1
2. Take plate, ring 2
3. Return plate, ring 2
4. Return plate, ring 1

And G2's eating sequence is
1. Take plate, ring 2
2. Take plate, ring 1
3. Return plate, ring 2
4. Return plate, ring 1

They may deadlock! And since in the real program we have 15 hungry guests who
dine in 5 rings, they always seem to deadlock :(

We need to find an arrangement that will prevent a deadlock.

## Building a Food Dependency Graph
We had a few failed attempts to solve this thing, and building a graph was the
first approach that seemed promising. We turned all plates into graph nodes and
all food dependencies into directed edges. If a guest grabs a plate from ring 2
while already holding a plate from ring 1, we will make an edge from plate 2 to
1, to signal that plate 2 depends on plate 1.

If we have one guest with the following sequence, we will produce this graph:
(+1 means that a guest takes a plate from ring 1, -1 means that a guest returns
a plate to ring 1)

![](simple_1.png)

If we have two guests, we will produce this graph:

![](simple_2.png)

In this case, which may deadlock, we will produce this graph:

![](simple_2_deadlock.png)

Notice that we have a loop! This means that we may deadlock. Therefore, a
seating arrangement where guests A and B share a plate in ring 1, 2 and 3 is
unacceptable.

Once we have this way to rule out bad arrangements, we can try to recursively
find a good one:
- Seat guest A on seat 1, which assigns them to plates in rings 0-4, add their
  dependency edges to the plates.
- Seat guest B on seat 2, add their edges.
- ... Continue while there are no loops in the graph
- If a loop is created, for example by seating guest E in seat 6, try to find
  another seat for guest E. If no seat can be found, we'll have to backtrack to
  find another sit for guest D, etc...
- Repeat until all guests are seated

In code, it looks something like this:

```python
def seat (arrangement, person_i):
    if person_i == -1:
        return True # No more people to seat
    for seat_i in range (15):
        if not seat_free (arrangement, seat_i):
            continue
        plates = get_plates_for_seat (seat_i)
        if add_to_graph (person_i, plates):
            # Added person without any loops
            arrangement[person_i] = seat_i
            # Seat remaining people
            if seat (arrangement, person_i - 1):
                return True
            # We couldn't sit all people in this arrangement without creating a
            # loop. Try another seat for this person.
            remove_from_graph (person_i, plates)
            arrangement[person_i] = -1

    # Nothing could be found
    return False
```

I believe the cool guys call this "backtracking depth-first search".

## Issue 1 - Loops created by the same guest
Consider the following sequence:

![](same_guest_loop.png)

It has a loop, but it's clearly not a problem, since one guest can't deadlock
with himself.

The solution was to mark each edge with the guest that created it. Then, when
searching for loops while adding edges to the graph, we ignore edges created by
the same guest whose edges we're adding now. This way loops that were created by
the guest with himself do not count as loops.

![](same_guest_annotate.png)

After solving this issue, the algorithm was able to find seating arrangements
for almost all guests! But it still couldn't figure out the last guest... It
means we're too strict in our constraints and we have false positives for
deadlocks.

## Issue 2 - Loops with no deadlocks
So far we assumed that a loop in the graph means a deadlock. But this isn't
always true! Consider this sequence:

![](loop_no_deadlock.png)

We have a loop, but these guests can't deadlock because even though rings 2 and
3 are acquired in different order, they are both "protected" by ring 1, which is
acquired first.

This situation is a bit trickier to detect, but here's how we did it: When
adding an edge to the graph, store the order of all currently held plates on the
edge. Example:

1. Grab ring 2
2. Grab ring 3
3. Grab ring 1
4. Release ring 2
5. Grab ring 2
6. Grab ring 4

When processing instruction 6, we will know that we hold plates [3, 1, 2], in
that acquisition order. As previously, we will add an edge from plate 4 to plate
2 (to create a dependency) but we will also store the list [3, 1] on that edge
to document plates 3 and 1 were also held, in that order.

Then, while adding an edge for another guest, when traversing the graph while
searching for loops we will ignore edges that have a similar acquisition order
to the current guest.

For example, if the current guest grabbed plates in this order: [4,2,3,1], we
will ignore an edge that had this order: [2,1,3]. Why? Because the first common
plate that they both acquired was 2, which means that everything after that
doesn't matter, because plate 2 isn't released yet.

Another example, if the guest grabbed [4,2,3,1] and hit an edge [2,1,4], we will
*not* ignore it, because we see that the first common plate that this guest
grabbed was 4, while the first common plate that the edge's guest has grabbed
was 2. They are different so we must traverse this edge to check for loops.

If we return to the example at the beginning of the section, with acquisition
order information it will look like this:

![](loop_no_deadlock_annotate.png)

We can see that the loop between 2 and 3 can be ignored because it has a similar
plate acquisition order.

After adding this logic to the algorithm, it worked! An arrangement was found!

## Summing up
We expected this algorithm to take a while to run, so we coded the whole thing
in C++ to make use of the extra performance. But in the end it took only 2
seconds to find the arrangement so in hindsight we could have just used Python...

[The code is available in this repository.](graphs.cpp)

The computed arrangement was: 12 14 4 8 5 11 10 2 1 9 3 13 6 15 7

And the flag is:
`PCTF{J ljv3 Jn th3 1nt3rs3ctJon of CSP and s3curjty and partj3s!}`
