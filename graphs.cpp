#include <string>
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <assert.h>

using std::string;
using std::cout;
using std::vector;

#define CHECK(condition) assert (condition)

static const int MAX_EDGES = 30;
static const int MAX_ORDERS = 4;
struct Node;
struct AquireOrder {
    int count;
    int order[MAX_ORDERS];
};
struct Edge {
    Node * node;
    int person;
    AquireOrder order;
};
struct Node {
    char name[20];
    int column;
    int mark;
    int edges_count;
    Edge edges[MAX_EDGES];
};

static int g_mark = 1;

static Node g_food[] = {
    {"samosas", 0},
    {"pakoras", 3},
    {"samosa chaat", 6},
    {"samosas", 9},
    {"samosas", 12},

    {"dal makhani", 1},
    {"palak paneer", 6},
    {"matar methi malai", 11},
    {},
    {},

    {"mango lassi", 1},
    {"chaas", 4},
    {"mango lassi", 8},
    {"mango lassi", 14},
    {},

    {"basmati rice", 2},
    {"basmati rice", 8},
    {},
    {},
    {},

    {"garlic naan", 1},
    {"plain naan", 5},
    {"butter naan", 9},
    {"kashmiri naan", 10},
    {"roti", 13},
};

struct Move {
    bool claim;
    int row;
};

struct Perp {
    char name[20];
    vector<Move> moves;
};

static Perp g_perps[] = {
    {"awesie", {
            {true, 4},
            {true, 3},
            {false, 4},
            {true, 0},
            {false, 3},
            {false, 0},
            {true, 2},
            {true, 1},
            {false, 2},
            {false, 1},
            {true, 0},
            {true, 2},
            {false, 0},
            {true, 0},
            {false, 2},
            {false, 0},
        }},
    {"cai", {
            {true, 0},
            {true, 4},
            {true, 2},
            {true, 1},
            {false, 0},
            {false, 1},
            {true, 3},
            {false, 2},
            {false, 3},
            {true, 1},
            {false, 1},
            {false, 4},
        }},
    {"arye", {
            {true, 2},
            {true, 1},
            {false, 2},
            {true, 2},
            {false, 2},
            {true, 0},
            {false, 0},
            {true, 4},
            {false, 4},
            {true, 3},
            {false, 3},
            {false, 1},
            {true, 4},
            {false, 4},
        }},
    {"f0xtrot", {
            {true, 4},
            {true, 3},
            {false, 4},
            {true, 0},
            {false, 3},
            {false, 0},
            {true, 2},
            {true, 1},
            {false, 2},
            {true, 4},
            {false, 1},
            {false, 4},
        }},
    {"jarsp", {
            {true, 4},
            {false, 4},
            {true, 0},
            {false, 0},
            {true, 1},
            {true, 2},
            {true, 3},
            {false, 1},
            {false, 3},
            {false, 2},
            {true, 2},
            {true, 3},
            {false, 3},
            {false, 2},
            {true, 2},
            {false, 2},
        }},
    {"panda", {
            {true, 4},
            {true, 3},
            {false, 4},
            {true, 0},
            {false, 0},
            {true, 2},
            {false, 3},
            {true, 1},
            {false, 1},
            {false, 2},
            {true, 0},
            {true, 2},
            {false, 0},
            {false, 2},
        }},
    {"ricky", {
            {true, 0},
            {true, 2},
            {false, 0},
            {true, 1},
            {false, 1},
            {false, 2},
            {true, 3},
            {true, 0},
            {false, 0},
            {true, 4},
            {false, 3},
            {false, 4},
        }},
    {"ryan", {
            {true, 4},
            {false, 4},
            {true, 0},
            {false, 0},
            {true, 1},
            {true, 3},
            {true, 2},
            {false, 3},
            {false, 2},
            {false, 1},
            {true, 0},
            {false, 0},
        }},
    {"strikedkids", {
            {true, 0},
            {true, 2},
            {false, 2},
            {false, 0},
            {true, 0},
            {false, 0},
            {true, 4},
            {false, 4},
            {true, 2},
            {true, 1},
            {false, 1},
            {false, 2},
            {true, 0},
            {false, 0},
            {true, 3},
            {true, 4},
            {false, 4},
            {false, 3},
            {true, 0},
            {false, 0},
        }},
    {"susie", {
            {true, 3},
            {true, 1},
            {false, 1},
            {true, 4},
            {false, 4},
            {false, 3},
            {true, 0},
            {true, 2},
            {false, 0},
            {true, 1},
            {false, 2},
            {false, 1},
        }},
    {"tylerni7", {
            {true, 1},
            {true, 3},
            {true, 2},
            {false, 2},
            {false, 1},
            {true, 0},
            {false, 3},
            {false, 0},
            {true, 4},
            {false, 4},
        }},
    {"ubuntur", {
            {true, 4},
            {true, 1},
            {true, 2},
            {false, 1},
            {false, 4},
            {true, 1},
            {false, 1},
            {false, 2},
            {true, 0},
            {true, 1},
            {true, 2},
            {false, 0},
            {false, 1},
            {false, 2},
        }},
    {"waituck", {
            {true, 0},
            {false, 0},
            {true, 0},
            {true, 1},
            {false, 0},
            {true, 2},
            {true, 3},
            {false, 2},
            {false, 3},
            {false, 1},
            {true, 0},
            {false, 0},
            {true, 1},
            {true, 3},
            {false, 1},
            {false, 3},
            {true, 4},
            {false, 4},
        }},
    {"zaratec", {
            {true, 0},
            {true, 2},
            {false, 0},
            {true, 1},
            {false, 1},
            {false, 2},
            {true, 0},
            {true, 4},
            {false, 0},
            {false, 4},
        }},
    {"zwad3", {
            {true, 3},
            {true, 1},
            {true, 2},
            {false, 3},
            {false, 2},
            {false, 1},
            {true, 1},
            {true, 2},
            {false, 2},
            {true, 2},
            {false, 2},
            {false, 1},
        }},
};

struct Dependency {
    int from;
    int to;
    AquireOrder order;
};

static const int MAX_DEPENDENCY = 20;
struct PerpDeps {
    int deps_count;
    Dependency deps[MAX_DEPENDENCY];
} g_perps_deps[15] = {};

void compile_deps (Perp * perp, PerpDeps * deps)
{
    vector<int> aquired;
    for (auto& move : perp->moves) {
        if (move.claim) {
            if (!aquired.empty ()) {
                int last = aquired[aquired.size ()-1];
                CHECK (aquired.size ()-1 < MAX_ORDERS);
                AquireOrder order = {static_cast<int> (aquired.size ()-1)};
                std::copy (cbegin (aquired), cend (aquired)-1, order.order);

                CHECK (deps->deps_count < MAX_DEPENDENCY);
                deps->deps[deps->deps_count++] = {move.row, last, order};
            }

            aquired.push_back (move.row);
        } else {
            aquired.erase (
                std::remove (begin (aquired), end (aquired), move.row),
                end (aquired));
        }
    }
}

static Node * g_food_for_row_per_seat[15][5];

int dist (int perp_column, int food_column)
{
    int v1 = 2 * (perp_column - food_column + 15) % 30;
    int v2 = (2 * (food_column - perp_column + 15) + 1) % 30;
    return v1 < v2 ? v1 : v2;
}

void compile_food_per_row ()
{
    for (int seat_i = 0; seat_i < 15; seat_i++) {
        for (int row = 0; row < 5; row++) {
            auto closest = &g_food[5 * row];
            for (int food_i = 5 * row; food_i < 5 * (row+1); food_i++) {
                if (g_food[food_i].name[0] == '\0') continue;
                if (dist (seat_i, g_food[food_i].column) <= dist (seat_i, closest->column)) {
                    closest = &g_food[food_i];
                }
            }
            g_food_for_row_per_seat[seat_i][row] = closest;
        }
    }
}

static bool same_order (const AquireOrder * o1, const AquireOrder * o2)
{
    int first_o1;
    for (int i = 0; i < o1->count; i++) {
        for (int j = 0; j < o2->count; j++) {
            if (o1->order[i] == o2->order[j]) {
                first_o1 = i;
                goto out;
            }
        }
    }
    return false;
out:
    for (int i = 0; i < o2->count; i++) {
        for (int j = 0; j < o1->count; j++) {
            if (o2->order[i] == o1->order[j]) {
                return o1->order[first_o1] == o2->order[i];
            }
        }
    }
    return false;
}

static bool _find_edge (Node * current, Node * needle, int person, const AquireOrder * order)
{
    if (current == needle) return true;

    if (current->mark == g_mark) return false;
    current->mark = g_mark;

    for (int i = 0; i < current->edges_count; i++) {
        if (current->edges[i].person == person) continue;
        if (same_order (&current->edges[i].order, order)) continue;

        if (_find_edge (current->edges[i].node, needle, person, order)) {
            return true;
        }
    }
    return false;
}

static bool find_edge (Node * current, Node * needle, int person, const AquireOrder * order)
{
    g_mark++;
    return _find_edge (current, needle, person, order);
}

bool add_edge (Node * from, Node * to, int person, const AquireOrder * order)
{
    if (find_edge (to, from, person, order)) {
        // Would make a loop
        return false;
    }

    CHECK (from->edges_count < MAX_EDGES);
    from->edges[from->edges_count++] = {to, person, *order};
    return true;
}

void erase_edge (Node * from, Node * to)
{
    CHECK (from->edges_count > 0);
    CHECK (from->edges[from->edges_count - 1].node == to);
    from->edges[--from->edges_count] = {};
}

bool seat_free (int * seats, int seat_i)
{
    for (int i = 0; i < 15; i++) {
        if (seats[i] == seat_i) return false;
    }
    return true;
}

bool add_food_deps (Node ** food_for_row, PerpDeps * deps, int person_i, int * out_progress) {
    *out_progress = 0;
    for (int i = 0; i < deps->deps_count; i++) {
        Dependency *dep = &deps->deps[i];
        if (!add_edge (food_for_row[dep->from], food_for_row[dep->to], person_i, &dep->order)) {
            return false;
        }
        *out_progress = i + 1;
    }
    return true;
}

void erase_food_deps (Node ** food_for_row, PerpDeps * deps, int progress) {
    for (int i = progress - 1; i >= 0; i--) {
        Dependency *dep = &deps->deps[i];
        erase_edge (food_for_row[dep->from], food_for_row[dep->to]);
    }
}

int max = 20;
bool seat (int * seats, int person_i)
{
    if (person_i < max) {
        max = person_i;
        for (int i = 0; i < 15; i++) {
            if (seats[i] == -1) cout << "x ";
            else cout << seats[i] + 1 << " ";
        } cout << "\n";
    }
    if (person_i == -1) return true;

    int progress;
    for (int seat_i = 0; seat_i < 15; seat_i++) {
        if (!seat_free (seats, seat_i)) continue;
        auto food_for_row = g_food_for_row_per_seat[seat_i];
        // No paneer for ricky
        if (person_i == 6 && food_for_row[1]->column == 6) continue;
        // cout << "trying " << person_i << " at " << seat_i << "\n";
        if (add_food_deps (food_for_row, &g_perps_deps[person_i], person_i, &progress)) {
            seats[person_i] = seat_i;
            if (seat (seats, person_i - 1)) return true;
            seats[person_i] = -1;
        }
        erase_food_deps (food_for_row, &g_perps_deps[person_i], progress);
    }
    return false;
}

int main (int argc, char * argv[])
{
    cout << "[+] Compiling dependencies\n";
    for (int i = 0; i < 15; i++) {
        compile_deps (&g_perps[i], &g_perps_deps[i]);
    }
    cout << "[+] Compiling food\n";
    compile_food_per_row ();

    int seats[15] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
    if (seat (seats, 14)) {
        cout << "[+] Found\n";
    }
    cout << "[+] Done\n";
    return 0;
}
